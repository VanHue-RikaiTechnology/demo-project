-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: demo_project
-- ------------------------------------------------------
-- Server version	5.7.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idUser` int(10) unsigned NOT NULL,
  `idTinTuc` int(10) unsigned NOT NULL,
  `NoiDung` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_iduser_foreign` (`idUser`),
  KEY `comment_idtintuc_foreign` (`idTinTuc`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,10,93,'Bài viết này được','2016-06-09 10:20:45',NULL),(2,8,19,'Hay quá trời','2016-06-09 10:20:45',NULL),(3,2,22,'Tôi rất thích bài viết này','2016-06-09 10:20:45',NULL),(4,2,41,'Ý kiến của tôi khác so với bài này','2016-06-09 10:20:45',NULL),(5,6,50,'Tôi rất thích bài viết này','2016-06-09 10:20:45',NULL),(6,9,79,'Bài viết này được','2016-06-09 10:20:45',NULL),(7,5,94,'Bài viết này được','2016-06-09 10:20:46',NULL),(8,8,99,'Bài viết này được','2016-06-09 10:20:46',NULL),(9,7,22,'Bài viết này được','2016-06-09 10:20:46',NULL),(10,5,48,'Tôi chưa có ý kiến gì','2016-06-09 10:20:46',NULL);
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaitin`
--

DROP TABLE IF EXISTS `loaitin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loaitin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idTheLoai` int(10) unsigned NOT NULL,
  `Ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TenKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `loaitin_idtheloai_foreign` (`idTheLoai`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaitin`
--

LOCK TABLES `loaitin` WRITE;
/*!40000 ALTER TABLE `loaitin` DISABLE KEYS */;
INSERT INTO `loaitin` VALUES (1,1,'Giáo Dục','Giao-Duc',NULL,NULL),(2,1,'Nhịp Điệu Trẻ','Nhip-Dieu-Tre',NULL,NULL),(3,1,'Du Lịch','Du-Lich',NULL,NULL),(4,1,'Du Học','Du-Hoc',NULL,NULL),(5,2,'Cuộc Sống Đó Đây','Cuoc-Song-Do-Day',NULL,NULL),(6,2,'Ảnh','Anh',NULL,NULL),(7,2,'Người Việt 5 Châu','Nguoi-Viet-5-Chau',NULL,NULL),(8,2,'Phân Tích','Phan-Tich',NULL,NULL),(9,3,'Chứng Khoán','Chung-Khoan',NULL,NULL),(10,3,'Bất Động Sản','Bat-Dong-San',NULL,NULL),(11,3,'Doanh Nhân','Doanh-Nhan',NULL,NULL),(12,3,'Quốc Tế','Quoc-Te',NULL,NULL),(13,3,'Mua Sắm','Mua-Sam',NULL,NULL),(14,3,'Doanh Nghiệp Viết','Doanh-Nghiep-Viet',NULL,NULL),(15,4,'Hoa Hậu','Hoa-Hau',NULL,NULL),(16,4,'Nghệ Sỹ','Nghe-Sy',NULL,NULL),(17,4,'Âm Nhạc','Am-Nhac',NULL,NULL),(18,4,'Thời Trang','Thoi-Trang',NULL,NULL),(19,4,'Điện Ảnh','Dien-Anh',NULL,NULL),(20,4,'Mỹ Thuật','My-Thuat',NULL,NULL),(21,5,'Bóng Đá','Bong-Da',NULL,NULL),(22,5,'Tennis','Tennis',NULL,NULL),(23,5,'Chân Dung','Chan-Dung',NULL,NULL),(24,5,'Ảnh','Anh-TT',NULL,NULL),(25,6,'Hình Sự','Hinh-Su',NULL,NULL),(27,1,'Sennehi 12','','2016-06-11 01:43:27','2016-06-11 01:43:27');
/*!40000 ALTER TABLE `loaitin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slide`
--

DROP TABLE IF EXISTS `slide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slide`
--

LOCK TABLES `slide` WRITE;
/*!40000 ALTER TABLE `slide` DISABLE KEYS */;
INSERT INTO `slide` VALUES (1,'Phan Thị Mơ thử sức với sân khấu kịch','phan-thi-mo-1-3984-1490342174_490x294.jpg','phan-thi-mo-thu-suc-voi-san-khau-kich',NULL,NULL),(2,'Phạm Hương khoe giọng trong MV đầu tay','pham-huong-khoe-giong-trong-mv-dau-tay-1491286120_490x294.jpg','pham-huong-khoe-giong-trong-mv-dau-tay',NULL,NULL),(3,'Ngọc Trinh khác lạ với màu tóc mới','ngoc-trinh-khoe-hinh-anh-khac-la-voi-toc-moi-nhuom-1491274712_490x294.jpg','ngoc-trinh-khac-la-voi-mau-toc-moi',NULL,NULL);
/*!40000 ALTER TABLE `slide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `theloai`
--

DROP TABLE IF EXISTS `theloai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `theloai` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TenKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `theloai`
--

LOCK TABLES `theloai` WRITE;
/*!40000 ALTER TABLE `theloai` DISABLE KEYS */;
INSERT INTO `theloai` VALUES (1,'Xã Hội','Xa-Hoi',NULL,NULL),(2,'Thế Giới','The-Gioi',NULL,NULL),(3,'Kinh Doanh','Kinh-Doanh',NULL,NULL),(4,'Văn Hoá','Van-Hoa',NULL,NULL),(5,'Thể Thao','The-Thao',NULL,NULL),(6,'Pháp Luật','Phap-Luat',NULL,NULL),(7,'Đời Sống','Doi-Song',NULL,NULL),(8,'Khoa Học','Khoa-Hoc',NULL,NULL),(9,'Vi Tính','Vi-Tinh',NULL,NULL);
/*!40000 ALTER TABLE `theloai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tintuc`
--

DROP TABLE IF EXISTS `tintuc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tintuc` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TieuDe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TieuDeKhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TomTat` text COLLATE utf8_unicode_ci NOT NULL,
  `NoiDung` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Hinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NoiBat` int(11) NOT NULL DEFAULT '0',
  `SoLuotXem` int(11) NOT NULL DEFAULT '0',
  `idLoaiTin` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tintuc_idloaitin_foreign` (`idLoaiTin`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tintuc`
--

LOCK TABLES `tintuc` WRITE;
/*!40000 ALTER TABLE `tintuc` DISABLE KEYS */;
INSERT INTO `tintuc` VALUES (1,'Lần đầu ĐH FPT cấp học bổng tiến sĩ ','Lan-Dau-Dh-Fpt-Cap-Hoc-Bong-Tien-Si','Bên cạnh 400 suất học bổng Nguyễn Văn Đạo, ĐH FPT lần đầu tiên chọn ra 30 học sinh xuất sắc nhất để cấp học bổng toàn phần đào tạo từ cử nhân lên thẳng tiến sĩ, với tổng giá trị quỹ lên tới 5 triệu USD.','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','FPT-2.jpg',1,0,1,NULL,NULL),(2,'300 tỷ đồng phát triển giáo dục mầm non ','300-ty-dong-phat-trien-giao-duc-mam-non','<p>aaaaaaaaaaaaaaa</p>\r\n','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','2.jpg',0,0,1,NULL,'2016-06-11 06:43:15'),(3,'Nợ giáo viên tiền tỷ chi phí phổ cập giáo dục ','No-Giao-Vien-Tien-Ty-Chi-Phi-Pho-Cap-Giao-Duc','Ba năm qua, nhiều giáo viên ở Khánh Hòa bỏ công sức, kể cả tiền bạc để thực hiện phổ cập giáo dục cho học sinh trên địa bàn tỉnh, song đến nay vẫn chưa nhận được tiền chính quyền chi trả. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','pho-cap-giao-duc-nho-2.jpg',1,0,1,NULL,NULL),(4,'Đón và chăm sóc trẻ sau giờ tan trường qua dịch vụ ','Don-Va-Cham-Soc-Tre-Sau-Gio-Tan-Truong-Qua-Dich-Vu','Các bé sẽ được chăm sóc bữa ăn, tắm rửa sạch sẽ, vui chơi và học tập cùng cô giáo theo các nội dung trong sổ báo bài, mở rộng hoặc đào sâu kiến thức theo yêu cầu của phụ huynh. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','be-2.jpg',1,0,1,NULL,NULL),(5,'7 học sinh rơi từ tầng hai xuống đất vì gãy lan can ','7-Hoc-Sinh-Roi-Tu-Tang-Hai-Xuong-Dat-Vi-Gay-Lan-Can','Đang giờ ra chơi, bất ngờ toàn bộ lan can tầng hai của Trường THCS thị trấn Chợ Rã (Bắc Kạn) gãy đổ ra ngoài, kéo theo 7 học sinh lớp 6A rơi xuống đất. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','tai_nan_set_top.gif',1,0,1,NULL,NULL),(6,'Giáo viên TP HCM được thưởng tết tối thiểu 700.000 đồng ','Giao-Vien-Tp-Hcm-Duoc-Thuong-Tet-Toi-Thieu-700.000-Dong','Sở GD&ĐT TP HCM vừa có thông báo về việc UBND thành phố chấp thuận đề nghị hỗ trợ mức quà tết cho cán bộ công chức trong ngành tối thiểu là 700.000 đồng. Mức thưởng này cao hơn năm ngoái 100.000 đồng. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','thuong-tet-3.jpg',1,0,1,NULL,NULL),(7,'Mức sinh hoạt phí tối đa cho lưu học sinh là 1.200 USD ','Muc-Sinh-Hoat-Phi-Toi-Da-Cho-Luu-Hoc-Sinh-La-1.200-Usd','Đối với lưu học sinh tại Ba Lan, Bungary, Nga..., mức sinh hoạt phí sẽ tăng từ 400 USD lên 480 USD; tại Australia, New Zealand tăng từ 860 USD lên 1.032 USD và tại Mỹ, Canada, Anh, Nhật Bản tăng từ 1.000 lên 1.200 USD một người một tháng... ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','du_hoc_sinh_set_sub.jpg',1,0,1,NULL,NULL),(8,'Học sinh Hà Nội được nghỉ 11 ngày Tết ','Hoc-Sinh-Ha-Noi-Duoc-Nghi-11-Ngay-Tet','UBND thành phố Hà Nội vừa đồng ý với đề xuất của Sở Giáo dục và Đào tạo về việc cho học sinh nghỉ tết Tết Nguyên đán Tân Mão 11 ngày. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','t2.jpg',1,0,1,NULL,NULL),(9,'Hàng trăm nghìn học sinh nghỉ học vì giá rét ','Hang-Tram-Nghin-Hoc-Sinh-Nghi-Hoc-Vi-Gia-Ret','Sớm nay, các trường tiểu học ở Hà Nội đều trưng biển thông báo nghỉ học do nhiệt độ ở mức 8 độ C. Một vài phụ huynh không theo dõi dự báo thời tiết vẫn đưa con đến trường và ngậm ngùi quay xe ra về. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','phu_huynh_xem_lich_nghi_hoc_set_sub.jpg',1,0,1,NULL,NULL),(10,'Phương pháp Mathnasium giúp trẻ yêu thích môn toán ','Phuong-Phap-Mathnasium-Giup-Tre-Yeu-Thich-Mon-Toan','Phương pháp dạy toán Mathnasium với 5 kỹ thuật giảng dạy bổ sung nhau, giúp trẻ em tiếp thu kiến thức toán hiệu quả, không cảm thấy áp lực và nhàm chán. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','hinh_250x195[1]_JPG_thumb210x0_ns.jpg',1,0,1,NULL,NULL),(11,'Những nụ hôn ngọt ngào trong đêm tình nhân ','Nhung-Nu-Hon-Ngot-Ngao-Trong-Dem-Tinh-Nhan','Tối 13/2, hàng nghìn bạn trẻ có mặt tại cầu Ánh Sao (quận 7, TP HCM) chứng kiến những lời tỏ tình cùng những nụ hôn ngọt ngào của 100 cặp tình nhân trong \"Đêm Valentine thế kỷ\". ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','250h_jpg_thumb210x0_ns.jpg',1,0,2,NULL,NULL),(12,'Hot girl tâm sự về ngày Valentine ','Hot-Girl-Tam-Su-Ve-Ngay-Valentine','Một bông hồng trắng bằng khăn giấy, chiếc xe đạp gắn đầy hoa, hay bài thơ của chàng \"thi sĩ\" vô danh gửi tặng… là những món quà đầy ấn tượng mà hot girl Midu từng nhận được trong các mùa Valentine. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','hot-girl-valentine-2.jpg',1,0,2,NULL,NULL),(13,'Nên duyên chồng vợ từ mạng mai mối ','Nen-Duyen-Chong-Vo-Tu-Mang-Mai-Moi','Quen nhau qua trang web kết bạn, để chiếm được tình cảm của cô nàng cao tới 1,71 m, chàng trai cao 1,58 m kiên trì tỏ tình tới 10 lần và hạnh phúc đã mỉm cười với họ. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','cap_doi_hoan_hao_set_sub.jpg',1,0,2,NULL,NULL),(14,'Những món quà Valentine làm từ tình yêu ','Nhung-Mon-Qua-Valentine-Lam-Tu-Tinh-Yeu','Cặm cụi cả tuần để thêu móc chìa khóa bằng len tặng người yêu, làm tranh bằng chính những hạt đỗ \"kỷ niệm ngày quen nhau\" của hai đứa là cách mà giới trẻ đang làm để tặng người yêu dịp lễ Valentine. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','socola_set_sub.jpg',1,0,2,NULL,NULL),(15,'100 cặp tình nhân hôn nhau trên khinh khí cầu ','100-Cap-Tinh-Nhan-Hon-Nhau-Tren-Khinh-Khi-Cau','100 cặp tình nhân sẽ trao nhau nụ hôn trên khinh khí cầu và được tặng một bó hoa với 999 nụ hồng xanh, nhận \"lời cầu hôn của thần Cupid\"... trong lễ hội Valentine sẽ được tổ chức tại cầu Ánh Sao (quận 7, TP HCM) tối 13/2. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','valentine22.jpg',1,0,2,NULL,NULL),(16,'Nhà thám hiểm 9 tuổi đặt chân tới Nam Cực ','Nha-Tham-Hiem-9-Tuoi-Dat-Chan-Toi-Nam-Cuc','Vượt qua hành trình dài nhiều ngày, Phạm Vũ Thiều Quang, cậu bé 9 tuổi đã cùng bố đặt chân tới Nam Cực vào chiều mùng 1 Tết. Cậu bé đã trở thành người châu Á trẻ tuổi nhất đặt chân tới vùng đất này. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','top-2.jpg',1,0,2,NULL,'0000-00-00 00:00:00'),(17,'Giới trẻ nô nức xin chữ đầu năm ','Gioi-Tre-No-Nuc-Xin-Chu-Dau-Nam','9h sáng mùng 4 Tết dòng người kéo đến Văn miếu Quốc Tử Giám đông nghẹt. Nhiều bạn trẻ đứng chen chân hàng tiếng đồng hồ để xin được chữ: Trạng nguyên, Đỗ đạt, Trí, Nhân... khi xuân về. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','tre-1.jpg',1,0,2,NULL,NULL),(18,'Mong ước đầu năm của giới trẻ ','Mong-Uoc-Dau-Nam-Cua-Gioi-Tre','Trong năm mới, chàng thủ khoa ĐH Ngoại thương Tăng Văn Bình quyết tâm trau dồi ngoại ngữ để thực hiện ước mơ du học, còn Miss Teen Diễm Trang sẽ dành nhiều thời gian hơn cho hoạt động xã hội, giao lưu quốc tế. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','diem-trang-2.jpg',1,0,2,NULL,NULL),(19,'Giám đốc tuổi mèo và thành tích đáng nể ','Giam-Doc-Tuoi-Meo-Va-Thanh-Tich-Dang-Ne','Học hết lớp 9, Nguyễn Hữu Năm phải nghỉ học vì nhà nghèo lại đông con, nhưng chàng trai đất Chương Mỹ (Hà Nội) đã xuất sắc giành nhiều giải thưởng sáng tạo trẻ và hiện là chủ công ty chuyên về chế tạo máy. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','Nguyen_Huu_Nam_set_sub.jpg',1,0,2,NULL,NULL),(20,'Học sinh vùng cao nghỉ Tết kéo dài vì giá rét','Hoc-Sinh-Vung-Cao-Nghi-Tet-Keo-Dai-Vi-Gia-Ret','Học sinh Hà Giang có thể nghỉ Tết Tân Mão gần 20 ngày còn học sinh Lào Cai được nghỉ Tết hơn các vùng khác 3 ngày để tránh giá rét. ','</h3>Nội dung tin tức: Khoá học Lập trình PHP tại trung tâm đào tạo tin học khoa phạm</h3>\r\n    	<p>\r\n    	<b>Hotline kỹ thuật <b>: 0967 908 907<br>\r\n    	<b>Hotline tư vấn kháo học <b>: 094 276 4080<br>\r\n    	<b>Địa chỉ </b>: 90 Lê Thị Riêng, P.Bến Thành, Q1, TPHCM<br>\r\n    	<b>Website</b>: khoapham.vn<br>\r\n    	<b>Học Online tại :</b>online.khoapham.vn<br>\r\n    	</p>','sapa9.jpg',1,0,1,NULL,NULL);
/*!40000 ALTER TABLE `tintuc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `adress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Văn Huệ','vanhue854@gmail.com','$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2','0947346687','Bình Thạnh','2019-11-28 16:21:07',NULL),(18,'long','long854@gmail.com','$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2','123123123','quận 8','2019-11-29 03:02:07',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-29 13:35:02
