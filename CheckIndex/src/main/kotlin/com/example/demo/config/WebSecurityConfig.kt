package com.example.demo.config

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
open class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Bean
    open fun passwordEncoder():PasswordEncoder {
        val bCryptPasswordEncoder:BCryptPasswordEncoder = BCryptPasswordEncoder()
        return bCryptPasswordEncoder
    }

    @Autowired
    lateinit var infoUser : InfoUser

    override fun configure( auth:AuthenticationManagerBuilder){
        auth.userDetailsService(userDetailsServiceBean())
                .passwordEncoder(passwordEncoder());
    }

    override fun configure( web:WebSecurity) {
        web.ignoring().antMatchers("/demo/assets/**","/bootstrap/**","/css/**","/images/**");
    }

    override fun configure(http: HttpSecurity){
        http.authorizeRequests().antMatchers("/login").permitAll()
                .and().authorizeRequests().antMatchers("/qr/**").permitAll()

        .and().antMatcher("/**")
                .authorizeRequests()

                .and().formLogin()
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .loginPage("/login")
                .defaultSuccessUrl("/admin",true)
                .failureUrl("/login")

                .and().logout()
                .logoutRequestMatcher(AntPathRequestMatcher("/logout**"))
                .logoutSuccessUrl("/login")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .permitAll()

                .and().userDetailsService(infoUser)
    }
}