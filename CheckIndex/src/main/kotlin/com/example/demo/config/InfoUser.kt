package com.example.demo.config

import com.example.demo.data.MESSAGE
import com.example.demo.data.UtilMsg
import com.example.demo.entity.UsersEntity
import com.example.demo.repository.UsersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
open class InfoUser : UserDetailsService {
    @Autowired
    lateinit var usersRepository: UsersRepository

    override fun loadUserByUsername(email: String): UserDetails {
       val user:UsersEntity = usersRepository.findByEmail(email)

        return User(user.email, user.password,true,true,true,true, AuthorityUtils.createAuthorityList("ROLE_USER"))
    }
}