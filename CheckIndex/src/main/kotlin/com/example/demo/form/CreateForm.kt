package com.example.demo.form

import javax.validation.constraints.NotNull

data class CreateForm(
        @NotNull
    val name: String = "",
        @NotNull
    val email: String = "",
        @NotNull
    val password: String = "",
        @NotNull
    val phone: String = "",
        @NotNull
    val adress: String = ""
)
