package com.example.demo.service

import com.example.demo.entity.TheLoai
import com.example.demo.repository.TheLoaiRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TheLoaiService {
    @Autowired
    lateinit var theLoaiRepository: TheLoaiRepository
    fun findAll(): List<TheLoai>
    {
        return theLoaiRepository.findAll()
    }
}