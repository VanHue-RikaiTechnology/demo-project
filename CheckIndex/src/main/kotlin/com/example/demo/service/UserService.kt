package com.example.demo.service

import com.example.demo.entity.UsersEntity
import com.example.demo.repository.UsersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserService {
    @Autowired
    lateinit var usersRepository: UsersRepository

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    fun findAll(): List<UsersEntity> {
        return usersRepository.findAll();
    }

    fun delete(id: Int)
    {
        return usersRepository.deleteById(id)
    }

    operator fun get(id: Int): UsersEntity {
        return usersRepository.findById(id).get()
    }

    fun save(usersEntity: UsersEntity) {
        usersRepository.save(usersEntity)
    }

}
