package com.example.demo.service

import com.example.demo.entity.TinTuc
import com.example.demo.repository.TinTucRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DetailService {
    @Autowired
    lateinit var tinTucRepository: TinTucRepository

    fun findById(id:Int): TinTuc
    {
        return tinTucRepository.findById(id)

    }
}