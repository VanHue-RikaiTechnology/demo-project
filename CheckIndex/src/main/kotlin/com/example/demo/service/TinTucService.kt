package com.example.demo.service

import com.example.demo.entity.TinTuc
import com.example.demo.repository.TinTucRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort


@Service
class TinTucService {
    @Autowired
    lateinit var tinTucRepository: TinTucRepository
    fun findAll(): Page<TinTuc>
    {
//        return tinTucRepository.findAll();
        val pageable = tinTucRepository.findAll(PageRequest.of(0,5, Sort.by("id")));
        return  pageable
    }
    fun findMostView(): Page<TinTuc>
    {
        val pageable = tinTucRepository.findAll(PageRequest.of(0,5, Sort.by("soluotxem").descending()));
        return pageable;

    }

}