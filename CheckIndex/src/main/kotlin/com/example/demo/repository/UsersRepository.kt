package com.example.demo.repository

import com.example.demo.entity.UsersEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UsersRepository: JpaRepository<UsersEntity, Int> {
        fun findByEmail(email:String):UsersEntity
}