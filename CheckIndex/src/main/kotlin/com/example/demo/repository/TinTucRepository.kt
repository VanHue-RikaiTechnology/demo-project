package com.example.demo.repository

import com.example.demo.entity.TinTuc
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TinTucRepository : JpaRepository<TinTuc, Long >
{
    fun findById(id:Int):TinTuc
}