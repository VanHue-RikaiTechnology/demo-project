package com.example.demo.entity

import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name="THELOAI")
data class TheLoai(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = 0,

        @get: NotEmpty
        @get: Column(name = "TEN")
        val ten: String = "",

        @get: NotEmpty
        @get: Column(name = "TENKHONGDAU")
        val tenkhongdau: String = ""
)