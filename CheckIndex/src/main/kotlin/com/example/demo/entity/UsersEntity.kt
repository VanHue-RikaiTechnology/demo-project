package com.example.demo.entity

import javax.persistence.*
import javax.validation.constraints.*

@Entity
@Table(name="USERS")
class UsersEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0

    @get: NotEmpty
    @get: Column(name = "NAME")
    var name: String = ""

    @get: NotEmpty
    @get: Column(name = "EMAIL")
    var email: String = ""

    @get: NotEmpty
    @get: Column(name = "PASSWORD")
    var password: String = ""

    @get: NotEmpty
    @get: Column(name = "PHONE")
    var phone: String = ""

    @get: NotEmpty
    @get: Column(name = "ADRESS")
    var adress: String = ""


}