package com.example.demo.entity


import javax.persistence.*
import javax.validation.constraints.NotEmpty

@Entity
@Table(name="TINTUC")
data class TinTuc(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Int = 0,

        @get: NotEmpty
        @get: Column(name = "TIEUDE")
        val tieude: String = "",

        @get: NotEmpty
        @get: Column(name = "TIEUDEKHONGDAU")
        val tieudekhongdau: String = "",

        @get: NotEmpty
        @get: Column(name = "TOMTAT")
        val tomtat: String = "",

        @get: NotEmpty
        @get: Column(name = "NOIDUNG")
        val noidung: String = "",

        @get: NotEmpty
        @get: Column(name = "SOLUOTXEM")
        val soluotxem: Int=0,

        @get: NotEmpty
        @get: Column(name = "HINH")
        val hinh: String = ""

)