package com.example.demo.controller

import com.example.demo.entity.UsersEntity
import com.example.demo.repository.UsersRepository
import com.example.demo.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.util.*

class UsersController {
    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var usersRepository: UsersRepository

    @GetMapping ("login")
    fun checkData(model : Model):String{
        var user: UsersEntity = usersRepository.findById(1).get()
        model.addAttribute("data",user)
        return "about"
    }
}