package com.example.demo.controller

import com.example.demo.entity.TinTuc
import com.example.demo.service.DetailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class DetailController {
    @Autowired
    lateinit var detailService: DetailService

    @GetMapping("detail")
    fun LoadTinTuc(@RequestParam id: Int, model: Model):String{
        var tinTuc: TinTuc = detailService.findById(id)
        model.addAttribute("info", tinTuc)
        return "detail"
    }
}