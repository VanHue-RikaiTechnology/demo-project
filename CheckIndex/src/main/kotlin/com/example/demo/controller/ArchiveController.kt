package com.example.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class ArchiveController {
    @RequestMapping("archive")
    fun LoadArchive(model: Model): String {
        return "archive"
    }
}