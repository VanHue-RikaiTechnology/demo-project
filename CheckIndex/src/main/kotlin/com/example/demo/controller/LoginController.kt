package com.example.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class LoginController {

    @RequestMapping( "login")
    fun LoadLogin(model: Model): String {
        return "login"
    }
}