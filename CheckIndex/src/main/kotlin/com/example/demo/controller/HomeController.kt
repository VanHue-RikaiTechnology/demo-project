package com.example.demo.controller

import com.example.demo.entity.TheLoai
import com.example.demo.entity.TinTuc
import com.example.demo.service.TheLoaiService
import com.example.demo.service.TinTucService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model

import org.springframework.web.bind.annotation.RequestMapping

@Controller
class HomeController {
    @Autowired
    lateinit var theLoaiService: TheLoaiService
    @Autowired
    lateinit var tinTucService: TinTucService

    @RequestMapping("/")
    fun loadIndex(model: Model):String{
        var theLoai: List<TheLoai> = theLoaiService.findAll()
        model.addAttribute("theLoai", theLoai)

        var tinTuc: List<TinTuc> = tinTucService.findAll().toList()
        model.addAttribute("tinTuc",tinTuc)

        var mostView: List<TinTuc> = tinTucService.findMostView().toList()
        model.addAttribute("mostView",mostView)
        return "index"
    }

}