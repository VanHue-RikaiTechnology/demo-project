package com.example.demo.controller

import com.example.demo.entity.UsersEntity
import com.example.demo.repository.UsersRepository
import com.example.demo.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@Controller
class EditController {

    @Autowired
    lateinit var usersRepository : UsersRepository

    @Autowired
    lateinit var userService: UserService

    @RequestMapping(value = ["/save"], method = [RequestMethod.POST])
    fun save(@ModelAttribute("user") usersEntity: UsersEntity): String {
        userService.save(usersEntity)
        return "redirect:/admin"
    }

}