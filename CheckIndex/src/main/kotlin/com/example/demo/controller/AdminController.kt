package com.example.demo.controller

import com.example.demo.entity.UsersEntity
import com.example.demo.form.CreateForm
import com.example.demo.repository.UsersRepository
import com.example.demo.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class AdminController {

    @Autowired
    lateinit var usersRepository: UsersRepository

    @Autowired
    lateinit var userService: UserService

    @RequestMapping("admin")
    fun loadAdmin(model: Model):String{
        var authentication = SecurityContextHolder.getContext().authentication
        var userDetails: UserDetails  = authentication.principal as UserDetails
        var userData:UsersEntity = usersRepository.findByEmail(userDetails.username)
        var tendangnhap:String = userData.name
        model.addAttribute("name",tendangnhap)

        val users: List<UsersEntity> = userService.findAll()
        model.addAttribute("users", users);

        return "admin"
    }

    @RequestMapping("/delete/{id}")
    fun deleteProduct(@PathVariable(name = "id") id : Int): String {
        userService.delete(id)
        return "redirect:/admin"
    }

    @GetMapping("create")
    fun add(model: Model):String{

        var authentication = SecurityContextHolder.getContext().authentication
        var userDetails: UserDetails  = authentication.principal as UserDetails
        var userData:UsersEntity = usersRepository.findByEmail(userDetails.username)
        var tendangnhap:String = userData.name
        model.addAttribute("name",tendangnhap)

        model.addAttribute("dataForm", CreateForm())
        return "create"
    }

    @PostMapping("create")
    fun doAdd(@Validated @ModelAttribute usersEntity: UsersEntity):String{
        usersRepository.save(usersEntity)
        return "redirect:/admin"
    }

    @RequestMapping("/edit/{id}")
    open fun editUser(@PathVariable(name = "id") id: Int): ModelAndView? {
        val mav = ModelAndView("edit")
        val user: UsersEntity = userService.get(id)
        mav.addObject("user", user)
        return mav
    }

}