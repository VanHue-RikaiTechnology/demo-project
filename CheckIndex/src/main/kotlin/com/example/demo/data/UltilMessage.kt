package com.example.demo.data

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.stereotype.Component

enum class MESSAGE(val code: String) {
    FM_C001_1("FM-C001-1"),
    FM_C001_2("FM-C001-2"),
    FM_C002_1("FM-C002-1"),
    FM_C002_2("FM-C002-2"),
    FM_C002_3("FM-C002-3"),
    FM_C003_1("FM-C003-1"),
    FM_C003_2("FM-C003-2"),
    FM_C003_3("FM-C003-3"),
    FM_C003_4("FM-C003-4"),
    FM_C004_1("FM-C004-1"),
    FM_C004_2("FM-C004-2"),
    FM_C004_3("FM-C004-3"),
    FM_C004_4("FM-C004-4"),
    FM_C005_1("FM-C005-1"),
    FM_C005_2("FM-C005-2"),
    FM_C005_3("FM-C005-3"),
    FM_C005_4("FM-C005-4"),

    FM_S001_1("FM-S001-1"),
    FM_S001_2("FM-S001-2"),
    FM_S002_1("FM-S002-1"),
    FM_S002_2("FM-S002-2"),
    FM_S002_3("FM-S002-3"),
    FM_S003_1("FM-S003-1"),
    FM_S003_2("FM-S003-2"),
    FM_S003_3("FM-S003-3"),
    FM_S003_4("FM-S003-4"),
    FM_S004_1("FM-S004-1"),
    FM_S004_2("FM-S004-2"),
    FM_S004_3("FM-S004-3"),
    FM_S004_4("FM-S004-4"),
    FM_S005_1("FM-S005-1"),
    FM_S005_2("FM-S005-2"),
    FM_S005_3("FM-S005-3"),
    FM_S005_4("FM-S005-4")
}

@Component
class UtilMsg {
    @Autowired
    lateinit var messageSource: MessageSource

    fun getMessage(msgCode: MESSAGE): String {
        return messageSource.getMessage(msgCode.code, emptyArray<Any>(), LocaleContextHolder.getLocale())
    }
}


